<?php

namespace Yeknava\SimpleTicketing;

use ReflectionClass;
use Illuminate\Http\Request;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function list()
    {
        return SimpleTicket::orderBy('id', 'desc')
            ->paginate(config('simple-ticketing.list_paginate', 15));
    }

    public function show($id)
    {
        return SimpleTicket::find($id);
    }

    public function categories()
    {
        return config('simple-ticketing.categories');
    }

    public function priorities()
    {
        return config('simple-ticketing.priorities');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => ['nullable', 'string', 'max:100'],
            'body' => ['required', 'string', 'max:1000'],
            'category' => ['nullable', 'string', 'max:100'],
            'priority' => ['nullable', 'integer', 'digits:1'],
        ];

        $forceCheck = config('simple-ticketing.force_check_fields');
        if ($forceCheck) {
            $categories = config('simple-ticketing.categories');
            $validCategories = '';
            foreach ($categories as $key => $cat) {
                $validCategories .= $key === 0 ? $cat['value'] : ',' . $cat['value'];
            }
            $rules['category'] = ['required', 'in:' . $validCategories];

            $priorities = config('simple-ticketing.priorities');
            $validPriorities = '';
            foreach ($priorities as $key => $pri) {
                $validPriorities .= $key === 0 ? $pri['value'] : ',' . $pri['value'];
            }
            $rules['priority'] = ['required', 'in:' . $validPriorities];
        }

        $requiredInfo = config('simple-ticketing.required_user_info');
        if (in_array('email', $requiredInfo)) {
            $rules['email'] = ['required', 'email'];
        }

        if (in_array('phone', $requiredInfo)) {
            $rules['phone'] = array_merge(['required'], config('simple-ticketing.phone_field_rules', []));
        }

        if (in_array('name', $requiredInfo)) {
            $rules['name'] = ['required', 'string'];
        }

        $data = $request->validate($rules);
        $storeInDb = config('simple-ticketing.store_in_database');

        if (!empty($storeInDb) && $storeInDb == true) {
            $st = SimpleTicket::makeNew($data, auth()->user());
        }

        $ticketService = config('simple-ticketing.new_ticket_callback_service');
        if ($ticketService instanceof TicketServiceInterface) {
            $service = new ReflectionClass($ticketService);
            $service->newInstance()->onNewTicket($data);
        }

        $adminEmail = config('simple-ticketing.admin_email');
        if (!empty($adminEmail)) {
            (new AnonymousNotifiable)->route('mail', $adminEmail)
                ->notify(new TicketNotify($data, true));
        }

        return response()->json([
            'ticket_id' => $storeInDb ? $st->id : null,
            'status' => 'success'
        ]);
    }

    public function respond(Request $request, $id)
    {
        $refer = SimpleTicket::find($id);
        if (!$refer) {
            throw new \Exception('refer ticket did not found');
        }

        $rules = [
            'body' => ['required', 'string', 'max:1000'],
        ];

        $data = $request->validate($rules);

        $refer->respond_message = $data['body'];

        if ($user = auth()->user()) {
            $refer->respond_by()->associate($user);
        }

        $refer->respond_via = 'app';

        if (config('simple-ticketing.respond_email_notify') && !empty($refer->email)) {
            (new AnonymousNotifiable)->route('mail', $refer->email)
                ->notify(new TicketNotify([
                    'title' => $refer->title,
                    'body' => $data['body']
                ]));

            $refer->respond_via = 'mail';
        }
        $refer->save();

        return response()->json([
            'ticket_id' => $refer->id,
            'status' => 'success'
        ]);
    }

    protected function reply(Request $request, $id)
    {
        $refer = SimpleTicket::find($id);
        if (!$refer) {
            throw new \Exception('refer ticket did not found');
        }

        $rules = [
            'body' => ['required', 'string', 'max:1000'],
        ];

        $data = $request->validate($rules);

        $data['priority'] = $refer->priority;
        $data['category'] = $refer->category;

        if (!empty($refer->title)) {
            $data['title'] = 'reply: ' . $data['title'];
        }

        return [
            'ticket' => SimpleTicket::makeNew($data, auth()->user(), $refer),
            'refer' => $refer
        ];
    }

    public function userReply(Request $request, $id)
    {
        $ticket = $this->reply($request, $id)['ticket'];

        $adminEmail = config('simple-ticketing.admin_email');
        if (!empty($adminEmail)) {
            (new AnonymousNotifiable)->route('mail', $adminEmail)
                ->notify(new TicketNotify([
                    'title' => $ticket->title,
                    'body' => $ticket->body
                ], true));
        }

        return response()->json([
            'ticket_id' => $ticket->id,
            'status' => 'success'
        ]);
    }

    public function adminReply(Request $request, $id)
    {
        $data = $this->reply($request, $id);

        if (config('simple-ticketing.respond_email_notify') && !empty($data['refer']['email'])) {
            (new AnonymousNotifiable)->route('mail', $data['refer']['email'])
                ->notify(new TicketNotify([
                    'title' => $data['ticket']['title'],
                    'body' => $data['ticket']['body']
                ]));
        }

        return response()->json([
            'ticket_id' => $data['ticket']['id'],
            'status' => 'success'
        ]);
    }
}
