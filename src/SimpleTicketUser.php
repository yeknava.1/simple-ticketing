<?php

namespace Yeknava\SimpleTicketing;

use Illuminate\Database\Eloquent\Model;

class SimpleTicketUser extends Model
{
    protected $fillable = [
    ];

    public function ticket() {
        return $this->belongsTo(SimpleTicket::class);
    }

    public function user() {
        return $this->morphTo();
    }
}
