<?php

namespace Yeknava\SimpleTicketing;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketNotify extends Notification
{
    use Queueable;

    protected $data;
    protected $showAction;

    public function __construct(array $data, bool $showAction = false)
    {
        $this->data = $data;
        $this->showAction = $showAction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = config('app.name');
        if(!empty($this->data['title'])) {
            $subject .= ': '.$this->data['title'];
        }

        $msg = (new MailMessage)
                    ->subject($subject)
                    ->line($this->data['body']);

        if($this->showAction) {
            if(isset($this->data['email'])) {
                $msg = $msg->action('email', 'mailto:'.$this->data['email']);
            }
            if(isset($this->data['phone'])) {
                $msg = $msg->action('phone', 'tel:'.$this->data['phone']);
            }
        }

        return $msg;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
