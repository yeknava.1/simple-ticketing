<?php

namespace Yeknava\SimpleTicketing;

use Illuminate\Support\ServiceProvider;

class SimpleTicketingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/simple-ticketing.php' => $this->app->configPath().'/simple-ticketing.php',

            dirname(__DIR__, 1) . '/migrations/' =>
                database_path('migrations'),
        ]);

        $useRoutes = config('simple-ticketing.use_routes');
        if ($useRoutes === true) {
            $this->loadRoutesFrom(__DIR__.'/routes.php');
        }
    }
}
