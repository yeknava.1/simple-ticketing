<?php
use Yeknava\SimpleTicketing\Controller;

$path = config('simple-ticketing.route_base_path', '/tickets');
$reply = config('simple-ticketing.reply');

Route::middleware(config('simple-ticketing.user_middlewares', []))->group(function () use ($path, $reply) {
    Route::get($path.'/categories', Controller::class.'@categories');
    Route::get($path.'/priorities', Controller::class.'@priorities');
    Route::post($path, Controller::class.'@store');
    if($reply) {
        Route::post($path.'/{id}/user-reply', Controller::class.'@userReply');
    }
});
Route::middleware(config('simple-ticketing.admin_middlewares', []))->group(function () use ($path, $reply) {
    Route::get($path, Controller::class.'@list');
    Route::get($path.'/{id}', Controller::class.'@show');
    Route::put($path.'/{id}/respond', Controller::class.'@respond');
    if($reply) {
        Route::post($path.'/{id}/admin-reply', Controller::class.'@adminReply');
    }
});

