<?php

namespace Yeknava\SimpleTicketing;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleTicketing\Exceptions\PermissionException;

class SimpleTicket extends Model
{
    const STATUS_OPEN = 'open';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_CLOSED = 'closed';

    protected $fillable = [
        'title', 'body', 'name', 'email',
        'phone', 'priority', 'category',
        'file'
    ];

    public function referTicket()
    {
        return $this->belongsTo(SimpleTicket::class, 'refer_ticket_id');
    }

    public function ticketReplies()
    {
        return $this->where('refer_ticket_id', $this->id);
    }

    public function ticketUsers()
    {
        return $this->hasMany(SimpleTicketUser::class, 'ticket_id');
    }

    public function source()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->morphTo();
    }

    public function assignedTo()
    {
        return $this->morphTo();
    }

    public function respondBy()
    {
        return $this->morphTo();
    }

    public static function makeNew(
        array $data,
        Model $user = null,
        Model $refer = null,
        bool $isAdmin = false,
        Model $source = null,
        array $participants = []
    ) {
        $st = new static($data);
        $st->status = static::STATUS_OPEN;

        if (!empty($user)) {
            $st->user()->associate($user);
        }

        if ($source) {
            $st->source()->associate($source);
        }

        if (!empty($refer)) {
            $st->referTicket()->associate($refer);
            $st->status = static::STATUS_IN_PROGRESS;

            if ($user && !$isAdmin) {
                $userTicket = $refer->ticketUsers()->where('user_id', $user->getKey())
                    ->where('user_type', $user->getMorphClass())
                    ->first();

                if (!$userTicket) throw new PermissionException();
            }

            if ($refer->source) {
                $st->source()->associate($refer->source);
            }
        }

        $st->save();
        if ($user) {
            $ticketUser = new SimpleTicketUser();
            $ticketUser->user()->associate($user);
            $ticketUser->ticket()->associate($st);
            $ticketUser->save();
        }
        $st->addTicketUsers($participants);

        return $st;
    }

    public function close(): self
    {
        $this->status = static::STATUS_CLOSED;
        $this->save();

        return $this;
    }

    public function addTicketUsers(array $users)
    {
        $ticketUsers = [];

        foreach ($users as $pt) {
            if ($pt instanceof Model) {
                $ticketUser = $this->ticketUsers()->where('user_id', $pt->getKey())
                    ->where('user_type', $pt->getMorphClass())
                    ->first();

                if (!$ticketUser) {
                    $ticketUser = new SimpleTicketUser();
                    $ticketUser->user()->associate($pt);
                    $ticketUsers[] = $ticketUser;
                }
            }
        }

        $this->ticketUsers()->saveMany($ticketUsers);
    }

    public static function findBySource(Model $source)
    {
        return static::where('source_type', $source->getMorphClass())
            ->where('source_id', $source->getKey())
            ->get();
    }
}
