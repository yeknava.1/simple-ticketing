<?php

namespace Yeknava\SimpleTicketing;

interface TicketServiceInterface {
    public function onNewTicket(array $data) : void;
}