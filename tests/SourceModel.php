<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SourceModel extends Model
{
    use SoftDeletes;

    protected $table = 'simple_ticket_test_sources';
}
