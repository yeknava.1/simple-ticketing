<?php

use Tests\UserModel;
use Tests\SourceModel;
use Orchestra\Testbench\TestCase;
use Yeknava\SimpleTicketing\SimpleTicket;
use Yeknava\SimpleTicketing\Exceptions\PermissionException;

class TicketTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('simple-ticketing', require_once(__DIR__ . '/../config/simple-ticketing.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleTicketing\SimpleTicketingServiceProvider::class,
        ];
    }

    public function init()
    {
        $user = (new UserModel([]));
        $user->save();

        $source = (new SourceModel([]));
        $source->save();

        return $user;
    }

    public function test()
    {
        $user = $this->init();

        $user2 = (new UserModel([]));
        $user2->save();

        $admin = (new UserModel([]));
        $admin->save();

        $refticket = SimpleTicket::makeNew([
            'title' => 'test title',
            'body' => 'test body'
        ], $user, null, false, SourceModel::first());

        $this->assertSame($refticket->title, 'test title');
        $this->assertSame($refticket->body, 'test body');
        $this->assertInstanceOf(SourceModel::class, $refticket->source);
        $this->assertInstanceOf(UserModel::class, $refticket->user);
        $this->assertInstanceOf(UserModel::class, $refticket->ticketUsers()->first()->user);


        $ticket = SimpleTicket::makeNew([
            'title' => 'test',
            'body' => 'test'
        ], $user, $refticket, false);

        try {
            $ticket = SimpleTicket::makeNew([
                'title' => 'test',
                'body' => 'test'
            ], $user2, $refticket, false);
        } catch (PermissionException $e) {
            $this->assertTrue(true);
        }

        $refticket->addTicketUsers([$user2]);

        $ticket = SimpleTicket::makeNew([
            'title' => 'test',
            'body' => 'test'
        ], $user2, $refticket, false);

        $this->assertInstanceOf(SourceModel::class, $ticket->source);
        $this->assertInstanceOf(UserModel::class, $ticket->user);
        $this->assertInstanceOf(SimpleTicket::class, $ticket->referTicket);

        $ticket = SimpleTicket::makeNew([
            'title' => 'test',
            'body' => 'test'
        ], $admin, $refticket, true);

        $this->assertInstanceOf(SourceModel::class, $ticket->source);
        $this->assertInstanceOf(UserModel::class, $ticket->user);
        $this->assertInstanceOf(SimpleTicket::class, $ticket->referTicket);
        $this->assertInstanceOf(
            SourceModel::class,
            SimpleTicket::findBySource(SourceModel::first())->first()->source
        );
    }
}
