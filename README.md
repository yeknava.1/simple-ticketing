# Laravel Simple Ticketing Package

Laravel Simple Ticketing Package lets you handle simple case of ticketing in your applications. everything defines in config file, even you may decide whether you need to store tickets in database or just like to send it through admin's email address.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple ticketing package.

```bash
composer require yeknava/simple-ticketing
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

## Config
```php
<?php

return [
    /*
     * route's base path that will be used for package specific routes.
     * (use 'php artisan route:list' to find out all paths)
     */
    'route_base_path' => 'tickets',

    'categories' => [
        [
            'label' => 'bug report',
            'value' => 'bug'
        ],
        [
            'label' => 'feature request',
            'value' => 'feature'
        ],
    ],

    'priorities' => [
        [
            'label' => 'low',
            'value' => 1
        ],
        [
            'label' => 'normal',
            'value' => 2
        ],
        [
            'label' => 'high',
            'value' => 3
        ],
    ],

    /*
     * admin's email address that will be used to notify him/her for each user tickets
     */
    'admin_email' => env('ST_ADMIN_EMAIL', ''),

    /*
     * specify fields that user required to fill for sending ticket
     */
    'required_user_info' => [
        'email',
        //'phone',
        //'name'
    ],

    /*
     * middleware checks that admin needs to pass to respond to ticket or reply one
     */
    'admin_middlewares' => ['auth:admin'],

    /*
     * middleware checks that user needs to pass to send ticket or reply one
     */
    'user_middlewares' => [],

    /*
     * if true it would check category and priority fields to match with values
     * defined in above to ensure values are in correct range, otherwise
     * users may specify category or priority fields by themselves
     */
    'force_check_fields' => false,

    /*
     * set this true if you need store tickets to database,
     * otherwise it would just pass to admin email
     */
    'store_in_database' => true,

    /*
     * Notify user via email whenever admin respond or reply (yes they are different!
     * each ticket may have only one respond message from admin and/or unlimited
     * reply from admin or user) to a ticket,
     * (tickets should store to database and email column should be filled)
     */
    'respond_email_notify' => true,

    /*
     * reply option allows admin and users continue their communication on same topic.
     */
    'reply' => true,

    'list_paginate' => 15
];

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## License
[MIT](https://choosealicense.com/licenses/mit/)
