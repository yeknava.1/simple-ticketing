<?php

namespace Yeknava\SimpleTicketing\Samples;

use Yeknava\SimpleTicketing\TicketServiceInterface;

class TicketServiceSample implements TicketServiceInterface {
    public function onNewTicket(array $data): void
    {
    }
}