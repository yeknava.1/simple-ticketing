<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 200)->nullable();
            $table->text('body');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('status')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('user_type')->nullable();
            $table->string('assigned_to_email')->nullable();
            $table->unsignedInteger('assigned_to_id')->nullable();
            $table->string('assigned_to_type')->nullable();
            $table->unsignedInteger('respond_by_id')->nullable();
            $table->string('respond_by_type')->nullable();
            $table->string('respond_via')->nullable();
            $table->text('respond_message')->nullable();
            $table->string('category')->nullable();
            $table->tinyInteger('priority')->unsigned()->nullable();
            $table->text('file')->nullable();
            $table->bigInteger('refer_ticket_id')->unsigned()->nullable();
            $table->foreign('refer_ticket_id')->references('id')->on('simple_tickets');
            $table->unsignedInteger('source_id')->nullable();
            $table->string('source_type')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_tickets');
    }
}
